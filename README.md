# How to use

- Must have python3
- Install redis and mongo:

```
pip install -r requirements.txt
```

# Run the code

Either

- Navigate one folder up and:

```bash
python3 -m folder_name # here is market
```

- Run the `main` method in `__main__.py` using pycharm, etc.