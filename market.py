from database import Redis, Mongo, Database


class Product:
    def __init__(self, pro_id, name, date, category, company):
        self.pro_id = pro_id
        self.name = name
        self.date = date
        self.category = category
        self.company = company

    def json(self):
        return {
            '_id': self.pro_id,
            'name': self.name,
            'date_created': self.date,
            'category': self.category,
            'company': self.company
        }


class Shop:
    def __init__(self, database='redis'):
        if database == 'redis':
            self.database: Database = Redis()
        elif database == 'mongo':
            self.database: Database = Mongo()
        else:
            raise TypeError('Database must be either mongo or redis')

    def insert_product(self, product: Product):
        self.database.add(product.json())

    def delete_product(self, pro_id):
        self.database.delete(pro_id)

    def update_product(self, product: Product):
        self.database.update(product.json())

    def get_all(self):
        return self.database.get_all()

    def get(self, pro_id):
        return self.database.get(pro_id)

    def sort(self):
        print('No idea what to do')
        pass
