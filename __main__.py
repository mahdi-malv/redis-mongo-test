from market import Shop, Product

def display_splash():
    print('[Version 1.0]:\nRemember to run Redis or MongoDB before usage')


if __name__ == '__main__':

    while True is True:
        database = input("Enter database name (mongo or redis): ")
        if database in ['redis', 'mongo']:
            break
        else:
            print(f'Enter redis or mongo. {database} is an invalid database')
    shop = Shop(database='redis')  # other option is 'mongo'

    actions = ['i', 'a', 'g', 'd', 'v', 'q']
    while True is not False:
        print("""
Enter the action
'i' for insert
'a' for get all products
'g' to get a specific one
'd' to delete a product
'v' to get database name
'q' to exit
""")
        action = input(f'Enter action from {actions}: ')
        if action not in actions:
            print(f'{action} is an invalid action. Enter from list')
            continue

        if action == 'i':
            product = Product(pro_id=input('Enter product id: '), name=input('Enter name: '), date=input('Enter the date:'), category=input('Enter category:'), company=input('Enter company:'))
            shop.insert_product(product)
        elif action == 'a':
            print(f'Returning all data saved in {database}')
            print(shop.get_all())
        elif action == 'g':
            print(shop.get(pro_id=input('Enter product id:')))   
        elif action == 'd':
            shop.delete_product(pro_id=input('Enter product id to delete:'))
        elif action == 'v':
            print(f'Database used is [{database}]')
        elif action == 'q':
            exit()
        else:
            raise ValueError(f'Invalid action {action}')

