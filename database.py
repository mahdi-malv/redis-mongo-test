from abc import ABC, abstractmethod
import redis
from pymongo import MongoClient, errors
import json

class Database(ABC):

    @abstractmethod
    def add(self, model):
        pass

    @abstractmethod
    def delete(self, pro_id):
        pass

    @abstractmethod
    def update(self, model):
        pass

    @abstractmethod
    def get_all(self):
        pass

    @abstractmethod
    def get(self, pro_id):
        pass


"""
Handle database ops using Mongo
"""
class Mongo(Database):

    def __init__(self):
        # make an instance using pymongo
        try:
            print('[Mongo]: Initializing')
            self.db = MongoClient().database.product
        except:
            raise ValueError('[Mongo]: Could not initialize database')

    def add(self, model):
        print(f'[Mongo]: Adding {model}')
        try:
            self.db.insert_one(model)
        except:
            the_id = model['_id']
            print(f'The product {the_id} seems to be existing already. Update instead of adding.')

    def delete(self, pro_id):
        print(f'[Mongo]: deleting {pro_id}')
        self.db.remove({ "_id" : pro_id})

    def update(self, model):
        print(f'[Mongo]: updating {model}')
        self.db.update_one({ "_id" : model.pro_id }, { "$set" : model })

    def get_all(self):
        print('[Mongo]: Getting all data')
        return list(self.db.find({}))

    def get(self, pro_id):
        print(f"[Mongo]: get {pro_id}")
        return self.db.find_one({ "_id" : pro_id})


"""
Handle database using Redis
"""
class Redis(Database):

    def __init__(self):
        self.name = 'pros'
        # Init localhost and stuff and make a variable
        print('[Redis] initializing')
        try:
            self.db = redis.Redis()
        except:
            raise ValueError('[Redis]: Could not initialize database')

    def add(self, model):
        print(f'[Redis]: adding {model}')
        self.db.set(model['_id'], json.dumps(model))

    def delete(self, pro_id):
        print(f'[Redis]: deleting {pro_id}')
        self.db.delete(pro_id)

    def update(self, model):
        print(f'[Redis]: updating {model}')
        self.db.set(model['_id'], json.dumps(model))

    def get_all(self):
        print('[Redis]: Getting all data')
        all_elements = []
        print(self.db.keys())
        for i in self.db.keys():
            v = self.db.get(i.decode('utf-8'))
            all_elements.append(v)
        return all_elements

    def get(self, pro_id):
        print(f'[Redis]: get {pro_id}')
        return self.db.get(pro_id)
